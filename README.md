# Unraid 6.12 Custom Kernel

With Intel Arc Support.

- [Unraid 6.12 Custom Kernel](#unraid-612-custom-kernel)
  - [DISCLAIMER](#disclaimer)
  - [Guide](#guide)
    - [Backup your USB drive!](#backup-your-usb-drive)
    - [Preparing kernel](#preparing-kernel)
    - [Compiling kernel](#compiling-kernel)
    - [Creating bzmodules file](#creating-bzmodules-file)
    - [Updating the USB drive](#updating-the-usb-drive)
    - [Checking the results](#checking-the-results)
  - [Sources](#sources)
  - [Troubleshooting](#troubleshooting)
    - [No network connection](#no-network-connection)

## DISCLAIMER

**I am not responsible for any physical damage or data loss or any other kind of problem that may arise when following this guide!**

The main reason the official Unraid kernel is still 6.1.X is because at the time of release ZFS was not released for newer kernels.
You should not follow this guide if you are planning on using ZFS in any way.

## Guide

This guide assumes you are working in `unraid/` directory.
Every code block is executed in that directory unless specified otherwise by using the `cd` command.

```bash
mkdir unraid
cd unraid
```

### Backup your USB drive!

Create a complete backup of the whole USB drive!
This guide may or may not work and it is great to have a backup.

Even better would be creating an Unraid VM and testing the compiled kernel.

```bash
dd if=/dev/sdX of=backup.img bs=4M status=progress
```

Use [Win32 Disk Imager](https://win32diskimager.org/) on Windows.

### Preparing kernel

1. Download the kernel sources from [kernel.org](https://www.kernel.org/).

I used [linux-6.2.16.tar.xz](https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.2.16.tar.xz):

```bash
wget https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.2.16.tar.xz
```

2. Extract the sources.<br>

```bash
mkdir linux
tar -xvf linux-6.2.16.tar.xz -C linux --strip-components 1
```

3. Download the unRAIDServer.zip file from unraid and extract the patches from it into our kernel sources.
   We need the custom patches that unraid applies to the kernel.

```bash
wget https://unraid-dl.sfo2.cdn.digitaloceanspaces.com/stable/unRAIDServer-6.12.2-x86_64.zip
unzip unRAIDServer-6.12.2-x86_64.zip -d unRAIDServer
cd unRAIDServer
unsquashfs -d patches bzfirmware src
cd ..
cp -r unRAIDServer/patches/src/linux-*-Unraid/. linux/
```

4. Apply the copied patches.

> Pay attention, some patches may fail!<br>
> Resolve those patches yourself or don't apply those patches if you don't need them.

```bash
cd linux
find . -type f -iname '*.patch' -print0 | xargs -n1 -0 patch -p1 -i
```

5. Configure kernel.

For Intel Arc support ensure that `unraid/linux/.config` includes the following options and that they are not commented out:
   - `CONFIG_INTEL_MEI=m`
   - `CONFIG_INTEL_MEI_ME=m`
   - `CONFIG_INTEL_MEI_TXE=m`
   - `CONFIG_INTEL_MEI_GSC=m`
   - `CONFIG_INTEL_MEI_HDCP=m`
   - `CONFIG_INTEL_MEI_PXP=m`
   - `CONFIG_INTEL_MEI_WDT=m`

```bash
cd linux
make oldconfig
```

> I mostly used the default option for new config variables.

### Compiling kernel

1. Compile the kernel and save it for later (bzimage)

```bash
cd linux
make -j$(nproc) bzImage
make -j$(nproc)
make -j$(nproc) modules
```

2. Install the modules in a temporary directory.

```bash
cd linux
make INSTALL_MOD_PATH=../modules modules_install
```

### Creating bzmodules file

Since Unraid 6.12 the contents of this file changed.
[Release bz file differences](https://docs.unraid.net/unraid-os/release-notes/6.12.0/#release-bz-file-differences)

1. Extract the original bzmodules.

```bash
cd unRAIDServer
unsquashfs -d modules bzmodules
```

2. Replace modules in extracted folder.

```bash
rm -rf unRAIDServer/modules/modules/*-Unraid
cp -r modules/lib/modules/6.2.16-Unraid unRAIDServer/modules/modules
```

3. Download additional firmware for the DG2 architecture.

> Get all files starting with `dg2_`. Some of the files may already be present.

```bash
cd unRAIDServer/modules/firmware/i915
rm dg2_*.bin
wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/i915/dg2_dmc_ver2_06.bin
wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/i915/dg2_dmc_ver2_07.bin
wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/i915/dg2_dmc_ver2_08.bin
wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/i915/dg2_guc_70.1.2.bin
wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/i915/dg2_guc_70.4.1.bin
wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/i915/dg2_guc_70.bin
wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/i915/dg2_huc_gsc.bin
```

1. Repack bzmodules.

```bash
cd unRAIDServer
mksquashfs modules bzmodules_new
```

5. Collect files

```bash
mkdir release
cp linux/arch/x86_64/boot/bzImage release/bzimage
cp unRAIDServer/bzmodules_new release/bzmodules
```

6. Create checksums.

```bash
cd release
sha256sum bzimage | cut -d " " -f 1 > bzimage.sha256
sha256sum bzmodules | cut -d " " -f 1 > bzmodules.sha256
```

### Updating the USB drive

Simply copy all files from `unraid/release` to the USB drive and replace the existing files.

### Checking the results

Boot up your system with the new kernel.
I had some problems booting without a monitor connected in the past using an Arc A380.
I am using a monitor or a HDMI dumm plug.

1. Check if the system bootet and check basics like network connectivity.
2. Make sure the GuC/HuC firmware loaded without any `FAIL` or `ERROR`.

```bash
dmesg | grep i915
cat /sys/kernel/debug/dri/0/gt/uc/guc_info
cat /sys/kernel/debug/dri/0/gt/uc/huc_info
```

3. Try to use the Arc GPU in a docker container (Check other guides on how to do that).

## Sources

- [Thor2002ro's kernels](https://github.com/thor2002ro/unraid_kernel)
- [Arc Forum Entry](https://forums.unraid.net/topic/129027-intel-arc-support)
- [Kernel Forum Entry #1](https://forums.unraid.net/topic/82625-kernelcustom-kernel-build-with-treaks-20200307-v683558419108naviveganfsv4r8125zen2/)
- [Kernel Forum Entry #1](https://forums.unraid.net/topic/86419-kernelunraid-kernel-update-510rc4-zenpowerit87corefeqamdgpujmb575dvbr8125openrgbreset-amd-gpuzfsdaxexfatntfs3nvidia-driver/?do=findComment&comment=887316)

## Troubleshooting

### No network connection

In case you don't get a network connection you can try to copy an original `config/network.cfg` to the USB drive.
This will reset all your network settings.

**Example config:**

```
USE_DHCP=no
IPADDR=10.0.0.1
NETMASK=255.255.255.0
GATEWAY=10.0.0.254
```
